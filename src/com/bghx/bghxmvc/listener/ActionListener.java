package com.bghx.bghxmvc.listener;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.bghx.bghxmvc.util.BghxMvcConfig;



public class ActionListener  implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

		System.out.println("系统已停止");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		String xmlPath = context.getInitParameter("bghxmvc-config");
		String tomcatPath = context.getRealPath("\\");
		try{
			//读取扫描，并保存到context中
			context.setAttribute("scan-path", BghxMvcConfig.getScanPath(tomcatPath+xmlPath));
			System.out.println(context.getAttribute("scan-path"));
			
		}catch(Exception e){
			System.out.println("出错：读取bghxmvc-config.xml出错！");
		}
		System.out.println("系统已经加载完成");
		
	}

}
