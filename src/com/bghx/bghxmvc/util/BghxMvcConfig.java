package com.bghx.bghxmvc.util;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.bcel.internal.generic.RET;
import com.sun.org.apache.bcel.internal.generic.RETURN;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class BghxMvcConfig {

	public static String getScanPath(String xmlPath) {
		String scanPath="";
		try {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(new File(xmlPath));
			Element rootElement = document.getRootElement();
			scanPath=rootElement.getChild("scan-path").getValue();
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return scanPath;
	}
}
