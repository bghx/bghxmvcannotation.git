package com.bghx.bussiness.form;

import com.bghx.bghxmvc.form.ActionForm;

public class LoginForm extends ActionForm  implements Comparable{

	
	private String username;
	private String password;
	
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public LoginForm() {
		
	}
	
	public String toString(){
		return "username="+this.username+"|| password="+this.password;
	}	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
