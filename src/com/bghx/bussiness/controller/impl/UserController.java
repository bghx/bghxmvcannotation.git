package com.bghx.bussiness.controller.impl;

import javax.servlet.http.HttpServletRequest;

import com.bghx.bghxmvc.annotation.Autowired;
import com.bghx.bghxmvc.annotation.Controller;
import com.bghx.bghxmvc.annotation.RequestMapping;
import com.bghx.bghxmvc.annotation.RequestParam;
import com.bghx.bussiness.service.UserService;

@Controller("user")
public class UserController {
	@Autowired("UserServiceImpl")
	private UserService us;
	
	@RequestMapping("login.do")
	public String toLogin(@RequestParam("username") String username,@RequestParam("password") String password) {
		if(us.checkUserLogin(username, password)) {
			return "成功登录";	
		}else {
			return "登录失败";
		}

	}		
}
