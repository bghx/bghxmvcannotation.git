package com.bghx.bussiness.service;

import com.bghx.bghxmvc.annotation.Service;
import com.bghx.bussiness.vo.UserInfo;


public interface UserService {
	boolean checkUserLogin(String username,String password);
	UserInfo getUserInfo();

}
